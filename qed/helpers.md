
# Fugleman Helpers

To use Fugleman helpers, you have to include them into your Sinatra application. 

~~~ruby
  include Fugleman::Helpers
~~~

Fugleman helpers generate stuff for your Sinatra app that you need anyway. For example, it helps you with link generation. 

~~~ruby
  link = link_to 'Text', 'test.html'
  link.assert == '<a href="test.html">Text</a>'
~~~