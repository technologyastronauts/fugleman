# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name     = "fugleman"
  s.version  = "0.1.8"
  s.summary  = "Helpers and stuff for working with sinatra. Really simple."
  s.homepage = "https://bitbucket.org/technologyastronauts/fugleman"
  s.authors  = ['Kaspar Schiess']
  s.email    = ["kaspar.schiess@technologyastronauts.ch"]
  
  s.description = <<-EOF
HTML and other helper functions, for working with sinatra. Except simple. 

Simplistic even. Caution, this stick has a pointed end. And it might be the
one you're holding. 
EOF

  s.files         = Dir['**/*']
  s.test_files    = Dir['test/**/*'] + Dir['spec/**/*']
  s.executables   = Dir['bin/*'].map { |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_runtime_dependency 'rack'  

  s.add_development_dependency 'ae'
  s.add_development_dependency 'qed'
end
