
require 'fugleman/namespace'

# This started out life as a copy of code found in padrino, but was heavily 
# simplified and then forked. 
#
# NOTE This includes none of the SafeBuffer madness. Watch your escaping. 
#
module Fugleman::Helpers

  def extract_options! args
    peek = args.last
    
    if peek.kind_of? Hash
      return args.pop
    end

    Hash.new
  end

  # -------------------------------------------------------------- form creation

  def form_namespace path, obj=nil, &block
    block.call(::Fugleman::Namespace.new(self, path, obj))
  end

  # --------------------------------------------------------------..---- linking

  # Creates a link element with given name, url and options.
  #
  def link_to(*args, &block)
    options  = extract_options!(args)
    name = block_given? ? '' : args.shift


    url = args.first || '#'
    options = {href: url}.merge(options)

    block_given? ? content_tag(:a, options, &block) : content_tag(:a, name, options)
  end

  # ------------------------------------------------------------------- partials
  def partial name, opts={}, &block
    slim name, { layout: false }.update(opts), &block
  end

  # -------------------------------------------------------------- tag rendering
  def input_tag(type, name=nil, options = {})
    tag(
      :input, 
      {name: name}.update(
        options.reverse_merge!(:type => type)))
  end
  def tag(name, options = nil, open = false)
    attributes = tag_attributes(options)
    "<#{name}#{attributes}#{open ? '>' : ' />'}"
  end
  def content_tag(name, content = nil, options = nil, &block)
    if block_given?
      options = content if content.is_a?(Hash)
      content = capture_html(&block)
    end

    attributes = tag_attributes(options)
    output = String.new
    output.concat "<#{name}#{attributes}>"
    output.concat content.to_s
    output.concat "</#{name}>"

    output
  end

  # --------------------------------------------------------------------- assets
  def image_tag(url, options={})
    options.reverse_merge!(:src => image_path(url))
    tag(:img, options)
  end
  def javascript_include_tag(*sources)
    options = {
      :type => 'text/javascript'
    }.update(extract_options!(sources).symbolize_keys)
    sources.flatten.inject(String.new) do |all,source|
      all << content_tag(:script, nil, { :src => asset_path('javascripts/'+source+'.js') }.update(options))
    end
  end

  def image_path(src)
    asset_path("images/" + src)
  end

  # Formats a path to an asset, including the assets timestamp for cache 
  # invalidation. 
  #
  def asset_path(source = nil)
    timestamp = asset_timestamp(source)
    result_path = uri_root_path(source)
    "#{result_path}#{timestamp}"
  end
  # Returns the timestamp mtime for an asset.
  #
  #   asset_timestamp("some/path/to/file.png") => "?154543678"
  #
  def asset_timestamp(file_path)
    return nil if file_path =~ /\?/ || (self.class.respond_to?(:asset_stamp) && !self.class.asset_stamp)
    public_path = self.class.public_folder if self.class.respond_to?(:public_folder)
    public_file_path = File.join(public_path, file_path) if public_path
    stamp = File.mtime(public_file_path).to_i if public_file_path && File.exist?(public_file_path)
    stamp ||= Time.now.to_i
    "?#{stamp}"
  end

  # ------------------------------------------------------------------- escapism
  def escape_html(text)
    Rack::Utils.escape_html(text)
  end
  alias h escape_html

  def escape_value(string)
    string.to_s.gsub(ESCAPE_REGEXP) { |c| ESCAPE_VALUES[c] }
  end

  # ---------------------------------------------------------- delayed rendering

  # For content_for, etc.. please use Sinatra::ContentFor, 
  # part of sinatra-contrib.

  # ------------------------------------------------------------------- captures

  def capture_html(*args, &block)
    # block.call(*args)
  end

  # ----------------------------------------------------------- foundation stuff

  # Returns the URI root of the application with optional paths appended.
  #
  #   uri_root_path("/some/path") => "/root/some/path"
  #   uri_root_path("javascripts", "test.js") => "/uri/root/javascripts/test.js"
  #
  def uri_root_path(*paths)
    root_uri = self.class.uri_root if self.class.respond_to?(:uri_root)
    File.join(ENV['RACK_BASE_URI'].to_s, root_uri || '/', *paths)
  end


private
  ESCAPE_VALUES = {
    "&" => "&amp;",
    "<" => "&lt;",
    ">" => "&gt;",
    '"' => "&quot;"
  }
  ESCAPE_REGEXP = Regexp.union(*ESCAPE_VALUES.keys)

  BOOLEAN_ATTRIBUTES = [
    :autoplay,
    :autofocus,
    :formnovalidate,
    :checked,
    :disabled,
    :hidden,
    :loop,
    :multiple,
    :muted,
    :readonly,
    :required,
    :selected,
    :declare,
    :defer,
    :ismap,
    :itemscope,
    :noresize,
    :novalidate
  ]

  def tag_attributes(options)
    return '' if options.nil?
    attributes = options.map do |k, v|
      next if v.nil? || v == false
      if v.is_a?(Hash)
        nested_values(k, v)
      elsif BOOLEAN_ATTRIBUTES.include?(k)
        %(#{k}="#{k}")
      else
        %(#{k}="#{escape_value(v)}")
      end
    end.compact
    attributes.empty? ? '' : " #{attributes * ' '}"
  end

  ##
  # Iterate through nested values.
  #
  def nested_values(attribute, hash)
    hash.map do |k, v|
      if v.is_a?(Hash)
        nested_values("#{attribute}-#{k.to_s.dasherize}", v)
      else
        %(#{attribute}-#{k.to_s.dasherize}="#{escape_value(v)}")
      end
    end * ' '
  end
end