
module Fugleman
class Namespace
  attr_reader :path

  def initialize context, path, obj
    @context = context
    @path = path
    @obj = obj
  end

  def input_tag type, name, opts={}
    opts = {value: value(name)}.merge(opts)
    @context.input_tag type, full_name(name), opts
  end

  def checkbox name, opts={}
    input_tag :checkbox, name, '', {
      placeholder: nil, 
      class: nil,
      value: 'true',
      checked: value(name) }.merge(opts)
  end
  def textarea name, opts={}
    @context.content_tag :textarea, 
      @context.h(value(name)), {
        name: full_name(name) }.merge(opts)
  end

  def full_name something
    @path.to_s + "[#{something}]"
  end
  def value name
    return nil unless @obj
    return @obj.send(name)
  end
end
end